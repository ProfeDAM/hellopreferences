package com.preferences.profe.hellopreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String MIS_PREFERENCIAS = "MisPreferencias";
    private String nombre, email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences prefs =
                getSharedPreferences(MIS_PREFERENCIAS,Context.MODE_PRIVATE);

         this.nombre = prefs.getString ("nombre", "");
         this.email = prefs.getString("email","");

        EditText etNombre = (EditText) findViewById (R.id.etNombre);
        EditText etEmail = (EditText) findViewById(R.id.etMail);

        etNombre.setText(nombre);
        etEmail.setText(email);

        Button btnFinish = (Button) findViewById(R.id.btnFinish);
        btnFinish.setOnClickListener(this);



    }

    @Override
    protected void onStop() {
        super.onStop();

        SharedPreferences prefs =
                getSharedPreferences(MIS_PREFERENCIAS, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();
        this.email = ((EditText) findViewById(R.id.etMail)).getText().toString();
        this.nombre = ((EditText) findViewById(R.id.etNombre)).getText().toString();

        editor.putString("email", this.email);
        editor.putString("nombre", this.nombre);
        editor.commit();

    }

    @Override
    public void onClick(View view) {
        finish();
    }
}
